import TopicFilter from "./topic";

type ClientHeader = { [key: string]: unknown };
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ListenerFunc = (event: any) => void;

export interface Event<D = unknown, M = unknown> {
  topic_name: string;
  account_id?: string;
  event_type: string;
  data: D;
  metadata?: M;
}

export interface ClientOptions {
  ws: string;
  api?: string;
  request?: RequestInit;
  headers?: ClientHeader | (() => ClientHeader) | (() => Promise<ClientHeader>);
}

export default class Client {
  options: ClientOptions;
  token: string;
  listeners: Map<string, [TopicFilter, ListenerFunc]>;
  socket?: WebSocket;

  constructor(options: ClientOptions) {
    this.options = {
      api: `${options.ws.replace("ws", "http")}/api`,
      ...options,
    };
    this.token = "";
    this.listeners = new Map();

    this.connect();
  }

  connect() {
    this.socket = new WebSocket(this.options.ws);

    this.socket.onopen = () => {
      this.socket?.send("/token");

      this.subscribeListeners();
    };

    this.socket.onmessage = (e) => {
      const [type, data] = e.data.split(/ (.+)/);

      switch (type) {
        case "token":
          this.token = data;
          break;

        case "event":
          const event = JSON.parse(data);

          this.listeners.forEach(([filter, fn]) => {
            if (filter.match(event.topic_name)) {
              fn(event);
            }
          });
          break;

        default:
          break;
      }
    };

    this.socket.onclose = () => {
      this.socket = undefined;
      this.token = "";

      setTimeout(() => {
        this.connect();
      }, 1000);
    };

    this.socket.onerror = () => {
      this.socket?.close();
    };
  }

  async getToken(): Promise<string> {
    return new Promise((resolve) => {
      const fn = () => {
        if (!this.token) {
          requestAnimationFrame(fn);

          return;
        }

        const base64Url = this.token.split(".")[1];
        const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        const jsonPayload = decodeURIComponent(
          atob(base64)
            .split("")
            .map((c) => {
              return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
        );

        const { exp } = JSON.parse(jsonPayload);
        const expiresIn = exp * 1000 - new Date().getTime();

        if (expiresIn * 0.9 < 0) {
          this.token = "";
          this.socket?.send("/token");

          requestAnimationFrame(fn);

          return;
        }

        resolve(this.token);
      };

      fn();
    });
  }

  async fetch(
    method: string,
    path: string,
    body: unknown
  ): Promise<{
    status: number;
    data: unknown;
  }> {
    const token = await this.getToken();
    const headers = await this.headers();

    let options: RequestInit = {
      method,
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Miao-Token": token,
        ...headers,
      }),
      body: JSON.stringify(body),
    };

    if (this.options.request) {
      options = { ...options, ...this.options.request };
    }

    try {
      const res = await fetch(`${this.options.api}/${path}`, options);
      const data = await res.json();

      if (!res.ok) {
        throw data;
      }

      return data;
    } catch (error) {
      throw { code: 0, message: error };
    }
  }

  async subscribe(
    topic_filter: string,
    fn: ListenerFunc
  ): Promise<() => Promise<void>> {
    await this.fetch("POST", "subscribe", { topic_filters: [topic_filter] });

    try {
      this.listeners.set(topic_filter, [new TopicFilter(topic_filter), fn]);
    } catch (error) {
      console.error(error);
    }

    return async (): Promise<void> => {
      this.listeners.delete(topic_filter);

      await this.fetch("DELETE", "unsubscribe", {
        topic_filters: [topic_filter],
      });
    };
  }

  async subscribeListeners(): Promise<void> {
    if (this.listeners.size > 0) {
      await this.fetch("POST", "subscribe", {
        topic_filters: [...this.listeners.keys()],
      });
    }
  }

  async headers(): Promise<{ [key: string]: unknown } | null> {
    if (!this.options.headers) {
      return null;
    }

    if (typeof this.options.headers === "object") {
      return this.options.headers;
    }

    if (typeof this.options.headers !== "function") {
      throw new Error("Invalid headers");
    }

    const res = this.options.headers();

    if (res instanceof Promise) {
      return await res;
    }

    return res;
  }
}
