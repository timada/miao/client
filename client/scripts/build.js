const esbuild = require("esbuild");
const { globPlugin } = require("esbuild-plugin-glob");

Promise.all(
  [
    {
      format: "esm",
      target: ["esnext"],
    },
    {
      outExtension: { ".js": ".cjs" },
      format: "cjs",
    },
  ].map((options) =>
    esbuild.build({
      ...options,
      outdir: "dist",
      entryPoints: ["src/**/*.ts"],
      watch: process.argv.indexOf("--watch") !== -1,
      plugins: [globPlugin()],
    })
  )
).catch((err) => {
  console.error(err);
  process.exit(1);
});
