import path from "path";
import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";

import { peerDependencies } from "./package.json";

export default defineConfig({
  plugins: [solidPlugin()],
  server: {
    proxy: {
      "/api": {
        target: "http://127.0.0.1:4455/miao",
        changeOrigin: true,
      },
      "/sessions/whoami": {
        target: "http://127.0.0.1:4433",
        changeOrigin: true,
      },
    },
  },
  build: {
    lib: {
      entry: path.resolve(__dirname, "src/lib.tsx"),
      name: "Miao",
    },
    rollupOptions: {
      external: Object.keys(peerDependencies),
      output: {
        globals: {
          "solid-js": "SolidJs",
        },
      },
    },
  },
  resolve: {
    alias: [
      { find: /^(lib|utils|client|hooks|topic)/, replacement: "/src/$1" },
      { find: /^(components)\//, replacement: "/src/$1/" },
    ],
  },
});
