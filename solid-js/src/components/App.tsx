import {
  Component,
  createEffect,
  createResource,
  createSignal,
  For,
} from "solid-js";

import { createStore } from "solid-js/store";
import { produce } from "immer";
import { useSubscribe } from "lib";

import "./App.css";
import { useClient } from "hooks";

interface TodoGroup {
  title: string;
  items: Optimistic<Todo>[];
}

interface Todo {
  id: number;
  text: string;
  done: boolean;
}

interface Optimistic<T> {
  value: T;
  loading: boolean;
}

interface State {
  todos: TodoGroup[];
  selected: number;
}

const App: Component = () => {
  const [user] = createResource(async () => {
    const resp = await fetch("/sessions/whoami");
    const data = await resp.json();

    return data.identity.id;
  });

  const [inputValue, setInputValue] = createSignal("");
  const [state, setState] = createStore<State>({
    todos: [],
    selected: 0,
  });

  const miao = useClient();

  useSubscribe<Todo, { title: string }>("todo/+", (event) => {
    setState("todos", (todos) =>
      produce(todos, (draft: TodoGroup[]) => {
        const group = draft.find((t) => t.title === event.metadata?.title);
        const item = group?.items.find((t) => t.value.id === event.data.id);

        if (item) {
          item.loading = false;
          item.value = event.data;
        } else {
          group?.items.push({ loading: false, value: event.data });
        }
      })
    );
  });

  createEffect(() => {
    const todoKey = `${user()}__todos`;

    if (!localStorage[todoKey]) {
      localStorage[todoKey] = JSON.stringify({
        todos: [
          {
            title: "Productivity",
            items: [],
          },
          { title: "Assignments", items: [] },
          { title: "Work", items: [] },
        ],
        selected: 0,
      });
    }

    setState(JSON.parse(localStorage[todoKey]));
  });

  const onKeydown = (title: string, e: KeyboardEvent) => {
    const text = inputValue().trim();
    if (e.key !== "Enter" || !text) {
      return;
    }

    const todoKey = `${user()}__todos`;
    const id = new Date().getTime();

    setTimeout(() => {
      const value = { id, text, done: false };
      const updatedState = produce(
        JSON.parse(localStorage[todoKey]),
        (draft: State) => {
          draft.selected = state.selected;
          const group = draft.todos.find((t) => t.title === title);
          group?.items.push({
            loading: false,
            value,
          });
        }
      );

      localStorage[todoKey] = JSON.stringify(updatedState);

      miao.fetch("POST", "publish", {
        topic_name: `todo/${id}`,
        account_id: String(user()),
        event_type: "added",
        data: value,
        metadata: {
          title,
        },
      });
    }, Math.floor(Math.random() * (3000 - 1000) + 1000));

    setState("todos", (todos) =>
      produce(todos, (draft: TodoGroup[]) => {
        const group = draft.find((t) => t.title === title);
        group?.items.push({
          loading: true,
          value: { id, text, done: false },
        });
      })
    );

    setInputValue("");
  };

  const onDoneToggle = ({ title, todo }: { title: string; todo: Todo }) => {
    const todoKey = `${user()}__todos`;

    setTimeout(() => {
      const value = { ...todo, done: !todo.done };
      const updatedState = produce(
        JSON.parse(localStorage[todoKey]),
        (draft: State) => {
          draft.selected = state.selected;
          const item = draft.todos
            .find((t) => t.title === title)
            ?.items.find((i) => i.value.id === todo.id);

          if (item) {
            item.value = value;
          }
        }
      );

      localStorage[todoKey] = JSON.stringify(updatedState);

      miao.fetch("POST", "publish", {
        topic_name: `todo/${todo.id}`,
        account_id: String(user()),
        event_type: "updated",
        data: value,
        metadata: {
          title,
        },
      });
    }, Math.floor(Math.random() * (3000 - 1000) + 1000));

    setState("todos", (todos) =>
      produce(todos, (draft: TodoGroup[]) => {
        const item = draft
          .find((t) => t.title === title)
          ?.items.find((i) => i.value.id === todo.id);

        if (item) {
          item.loading = true;
          item.value.done = !todo.done;
        }
      })
    );
  };

  return (
    <div class="app">
      <div class="app__todo-list">
        <For each={state.todos}>
          {(group, i) => (
            <div
              class="app__todo-group"
              classList={{
                "app__todo-group--selected": state.selected === i(),
              }}
            >
              <div
                class="app__todo-group-header"
                onClick={() => setState("selected", i())}
              >
                <div>
                  <span>
                    <svg
                      width="19"
                      height="15"
                      viewBox="0 0 19 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M0.92334 2.8972V12.8754C0.92334 13.4046 1.13359 13.9122 1.50785 14.2865C1.8821 14.6608 2.3897 14.871 2.91897 14.871H16.8884C17.4177 14.871 17.9253 14.6608 18.2995 14.2865C18.6738 13.9122 18.8841 13.4046 18.8841 12.8754V4.89284C18.8841 4.36356 18.6738 3.85596 18.2995 3.48171C17.9253 3.10745 17.4177 2.8972 16.8884 2.8972H10.9015L8.90588 0.901566H2.91897C2.3897 0.901566 1.8821 1.11182 1.50785 1.48607C1.13359 1.86033 0.92334 2.36793 0.92334 2.8972Z"
                        fill="#413F3F"
                        fill-opacity="0.4"
                      />
                    </svg>
                  </span>
                  <span class="app__todo-group-title">{group.title}</span>
                </div>
                <div class="app__separator"></div>
                <div>
                  <span class="app__todo-group-arrow">
                    <svg
                      width="10"
                      height="6"
                      viewBox="0 0 10 6"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M4.96274 5.70863L0.729004 1.47489L2.14092 0.0639801L4.96274 2.8868L7.78457 0.0639801L9.19648 1.47489L4.96274 5.70863Z"
                        fill="#413F3F"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <ul>
                <For each={group.items}>
                  {(todo) => (
                    <li>
                      <input
                        id={`app__todo-item-checkbox-${todo.value.id}`}
                        type="checkbox"
                        disabled={todo.loading}
                        checked={todo.value.done}
                        onchange={[
                          onDoneToggle,
                          { title: group.title, todo: todo.value },
                        ]}
                      />
                      <label
                        for={`app__todo-item-checkbox-${todo.value.id}`}
                        aria-describedby="label"
                      >
                        {todo.value.text}
                      </label>
                    </li>
                  )}
                </For>
                <li class="app__add-todo-item">
                  <input
                    id="app__todo-item-checkbox-new"
                    type="checkbox"
                    disabled
                  />
                  <label
                    for="app__todo-item-checkbox-new"
                    aria-describedby="label"
                  >
                    &#8288;
                  </label>
                  <input
                    type="text"
                    placeholder="Write a task..."
                    value={inputValue()}
                    oninput={(e) => setInputValue(e.currentTarget.value)}
                    onkeydown={[onKeydown, group.title]}
                  />
                </li>
              </ul>
            </div>
          )}
        </For>
      </div>
    </div>
  );
};

export default App;
