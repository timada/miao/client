import { render } from "solid-js/web";
import { Provider } from "lib";

import "./index.css";

import App from "components/App";
import { Client } from "miao-client";

const client = new Client({
  ws: "ws://localhost:6750",
  api: "/api",
});

render(
  () => (
    <Provider client={client}>
      <App />
    </Provider>
  ),
  document.getElementById("root") as HTMLElement
);
